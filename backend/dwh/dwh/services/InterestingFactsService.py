import random

from dwh.db import session
from dwh.models.models import *
from dwh.models.models import SalesH


def get_interesting_fact():
    list_of_facts = ["El número total de tiendas en España es {{}}",
                     "El trimestre con menor cantidad de ventas es el {{}} de 2021",
                     "¿Sabías que la talla más vendida de todas es la {{}}?",
                     "En el mes de octubre de 2021 se vendieron {{}} en Madrid que en Barcelona",
                     "El color más de moda en el año 2021 fue el color {{}}",
                     "El tipo de prenda más vendido en Móstoles en 2020 fue {{}} ventas)",
                     "Llevamos {{}} años vendiendo ropa de la más alta calidad en España",
                     "El día con mayor volumen de ventas en Madrid y alrededores durante diciembre de 2021 fue el {{}} prendas vendidas",
                     "El día de Año Nuevo de 2021 se vendieron {{}} prendas",
                     "La comunidad autónoma con mayor cantidad de ventas durante la campaña de verano de 2021 fue {{}}"]
    # r = random.randint(1, 10)
    r = random.sample([1, 7], 1)[0]
    fact = list_of_facts[r - 1]
    query = get_fact(r)
    fact = fact.replace("{{}}", query)
    return fact


def get_fact(num):
    if num == 1:
        return str(session.query(ShopD).distinct(ShopD.cod_shop).count())
    elif num == 2:
        list_of_trimesters = [time.trimester for time in session.query(TimeD).distinct(TimeD.trimester).all()]
        list_of_sales = []
        for trimester in list_of_trimesters:
            list_of_cod_time_coincidences = [time.cod_time for time in session.query(TimeD)
                .filter(TimeD.year == 2021)
                .filter(TimeD.trimester == trimester).all()]
            max_day_of_trimester = max(list_of_cod_time_coincidences)
            min_day_of_trimester = min(list_of_cod_time_coincidences)
            sum_num_ventas_trimester = sum([sales.sales for sales in session.query(SalesH)
                                           .filter(SalesH.cod_time <= max_day_of_trimester)
                                           .filter(SalesH.cod_time >= min_day_of_trimester).all()])
            list_of_sales.append((sum_num_ventas_trimester, trimester))
        list_of_sales = sorted(list_of_sales, key=lambda trimester: trimester[0])
        return list_of_sales[0][1]
    elif num == 3:
        list_of_sizes = [product.size for product in session.query(ProductD).distinct(ProductD.size).all()]
        list_of_sales = []
        for size in list_of_sizes:
            list_of_cod_product_coincidences = [product.cod_product for product in session.query(ProductD)
                .filter(ProductD.size == size).all()]
            sum_num_ventas_trimester = sum([sales.sales for sales in session.query(SalesH)
                                           .filter(SalesH.cod_product.in_(list_of_cod_product_coincidences)).all()])
            list_of_sales.append((sum_num_ventas_trimester, size))
        list_of_sales = sorted(list_of_sales, key=lambda size: size[0])
        return list_of_sales[len(list_of_sales) - 1][1]
    elif num == 4:
        provinces = ["Madrid", "Barcelona"]
        list_of_sales_per_province = []
        for province in provinces:
            total_sales = sum(
                [sales.sales for sales in
                 session.query(SalesH).join(ShopD).join(VillageD).join(ProvinceD).join(TimeD) \
                     .filter(ProvinceD.name_province == province)
                     .filter(TimeD.month == 10)
                     .filter(TimeD.year == 2021).all()])
            list_of_sales_per_province.append((province, total_sales))

        list_of_sales_per_province = sorted(list_of_sales_per_province, key=lambda sales: sales[1])
        dif = list_of_sales_per_province[1][1] - list_of_sales_per_province[0][1]
        if list_of_sales_per_province[1][0] == "Madrid":
            return str(dif) + " más"
        else:
            return str(dif) + " menos"
    elif num == 5:
        colours = [product.colour for product in session.query(ProductD).distinct(ProductD.colour).all()]
        list_of_sales_per_colour = []
        for colour in colours:
            total_sales = sum([sales.sales for sales in session.query(SalesH).join(ProductD).join(TimeD)
                              .filter(TimeD.year == 2021)
                              .filter(ProductD.colour == colour).all()])
            list_of_sales_per_colour.append((colour, total_sales))
        list_of_sales_per_colour = sorted(list_of_sales_per_colour, key=lambda sales: sales[1])
        return list_of_sales_per_colour[len(list_of_sales_per_colour) - 1][0]
    elif num == 6:
        products = [product.name_prod for product in session.query(ProductD).distinct(ProductD.name_prod).all()]
        list_of_sales_per_product = []
        for product in products:
            total_sales = sum(
                [sales.sales for sales in session.query(SalesH).join(ShopD).join(VillageD).join(TimeD).join(ProductD) \
                    .filter(VillageD.city == "Móstoles")
                    .filter(TimeD.year == 2020)
                    .filter(ProductD.name_prod == product).all()])
            list_of_sales_per_product.append((product, total_sales))
        list_of_sales_per_product = sorted(list_of_sales_per_product, key=lambda sales: sales[1])
        return list_of_sales_per_product[len(list_of_sales_per_product) - 1][0] + " (" + str(
            list_of_sales_per_product[len(list_of_sales_per_product) - 1][1])
    elif num == 7:
        return str(session.query(TimeD).distinct(TimeD.year).count())
    elif num == 8:
        days = [time.cod_time for time in session.query(TimeD).distinct(TimeD.cod_time)
            .filter(TimeD.year == 2021)
            .filter(TimeD.month == 12).all()]
        list_sales_per_day = []
        for day in days:
            total_sales = sum(
                [sales.sales for sales in session.query(SalesH).join(TimeD).join(ShopD).join(VillageD)
                    .filter(VillageD.city == "Madrid")
                    .filter(TimeD.cod_time == day).all()])
            list_sales_per_day.append((day, total_sales))
        list_sales_per_day = sorted(list_sales_per_day, key=lambda sales: sales[1])
        return str(list_sales_per_day[len(list_sales_per_day) - 1][0]) + ", con " + str(
            list_sales_per_day[len(list_sales_per_day) - 1][1])
    elif num == 9:
        return str(sum([sales.sales for sales in session.query(SalesH).filter(SalesH.cod_time == 20210101).all()]))
    elif num == 10:
        list_of_cod_time_coincidences = [time.cod_time for time in session.query(TimeD)
            .filter(TimeD.year == 2021)
            .filter(TimeD.trimester == "Q2").all()]
        min_day = min(list_of_cod_time_coincidences)
        list_of_cod_time_coincidences = [time.cod_time for time in session.query(TimeD)
            .filter(TimeD.year == 2021)
            .filter(TimeD.trimester == "Q3").all()]
        max_day = max(list_of_cod_time_coincidences)
        regions = [region.name_region for region in session.query(RegionD).all()]
        list_of_sales_per_CA = []
        for region in regions:
            total_sales = sum(
                [sales.sales for sales in
                 session.query(SalesH).join(ShopD).join(VillageD).join(ProvinceD).join(RegionD).join(TimeD).join(
                     ProductD) \
                     .filter(RegionD.name_region == region)
                     .filter(TimeD.cod_time <= max_day)
                     .filter(TimeD.cod_time >= min_day).all()])
            list_of_sales_per_CA.append((region, total_sales))
        list_of_sales_per_CA = sorted(list_of_sales_per_CA, key=lambda sales: sales[1])
        return list_of_sales_per_CA[len(list_of_sales_per_CA) - 1][0]