from typing import List
from rest_framework.request import Request


class SalesFilterDTO:
    def __init__(self):
        # 1 -> Geography filters
        self.country = ""
        self.region = ""
        self.province = ""
        self.village = ""

        # 2 -> Date filters
        # Saved as strings with format dd-mm-yyyy
        # Retrieved via getters when required as yyyymmdd
        self.start_date = ""
        self.end_date = ""

        # 3 -> Product filters
        # Saved as strings that are made of a list of elements separated by comma
        # Retrieved via getters when required as list[str]
        self.prod_size = None
        self.prod_colour = None
        self.prod_prod = None
        self.prod_brand = None

    # Returns the name of the specified hierarchy (only one of them can be active in a request)
    def get_geography_hierarchy(self) -> str:
        if self.village:
            return "village"
        if self.province:
            return "province"
        if self.region:
            return "region"
        if self.country:
            return "country"

    # Returns the value of the specified geography filter (only one of them can be active in a request)
    def get_geography_filter_value(self) -> str:
        if self.village:
            return self.village
        if self.province:
            return self.province
        if self.region:
            return self.region
        if self.country:
            return self.country

    # Returns start date in DB format
    def get_start_date(self) -> str:
        start_date_as_array = self.start_date.split("-")
        if len(start_date_as_array) != 3:
            raise Exception()
        return ''.join(reversed(start_date_as_array))

    # Returns end date in DB format
    def get_end_date(self) -> str:
        end_date_as_array = self.end_date.split("-")
        if len(end_date_as_array) != 3:
            raise Exception()
        return ''.join(reversed(end_date_as_array))

    # Returns product's size filter as list of string elements
    def get_prod_size(self):
        if self.prod_size is None:
            return None
        return self.prod_size.split(",")

    # Returns product's colour filter as list of string elements
    def get_prod_colour(self):
        if self.prod_colour is None:
            return None
        return self.prod_colour.split(",")

    # Returns product's type of cloth filter as list of string elements
    def get_prod_prod(self):
        if self.prod_prod is None:
            return None
        return self.prod_prod.split(",")

    # Returns product's brand filter as list of string elements
    def get_prod_brand(self):
        if self.prod_brand is None:
            return None
        return self.prod_brand.split(",")

    def fill_dto_from_request(self, request: Request):
        for attr in self.__dict__.keys():
            if attr in request.query_params:
                self.__setattr__(attr, request.query_params[attr])
        return self
