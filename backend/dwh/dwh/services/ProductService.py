from sqlalchemy import asc, distinct

from dwh.db import session
from dwh.models.models import ProductD


def get_all_products() -> list[ProductD]:
    """Gets all existing products in the database.
    Args:
    Return:
         list[ProductD]: List of products.
    """

    return session.query(ProductD).order_by(asc(ProductD.cod_product)).all()


def get_product_by_id(product_id: int) -> ProductD:
    """Gets existing product by identifier.
    Args:
        product_id(Integer): Product identifier.
    Return:
        ProductD: Searched product.
    """

    return session.get(ProductD, product_id)


def get_properties() -> dict[str, list[str]]:
    """Gets distinct properties of all products in the database.
    Args:
    Return:
        dict(str, list(str)): Dictionary with properties and values.
    """

    product_names = session.query(distinct(ProductD.name_prod)).order_by(asc(ProductD.name_prod)).all()
    product_colours = session.query(distinct(ProductD.colour)).order_by(asc(ProductD.colour)).all()
    product_sizes = session.query(distinct(ProductD.size)).order_by(asc(ProductD.size)).all()
    product_brands = session.query(distinct(ProductD.brand)).all()

    edit_product_names = []
    edit_product_colours = []
    edit_product_sizes = []
    edit_product_brands = []

    for elem in product_names:
        edit_product_names.append(elem._data[0])

    for elem in product_colours:
        edit_product_colours.append(elem._data[0])

    for elem in product_sizes:
        edit_product_sizes.append(elem._data[0])

    for elem in product_brands:
        edit_product_brands.append(elem._data[0])

    return {"names": edit_product_names,
            "colours": edit_product_colours,
            "sizes": edit_product_sizes,
            "brands": edit_product_brands}
