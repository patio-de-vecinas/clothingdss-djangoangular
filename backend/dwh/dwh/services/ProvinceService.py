from sqlalchemy import asc

from dwh.db import session
from dwh.models.models import ProvinceD


def get_all_provinces() -> list[ProvinceD]:
    """Gets all existing provinces in the database.
    Args:
    Return:
         list[ProvinceD]: List of provinces.
    """

    return session.query(ProvinceD).order_by(asc(ProvinceD.cod_province)).all()


def get_province_by_id(province_id: int) -> ProvinceD:
    """Gets existing province by identifier.
    Args:
        province_id(Integer): Province identifier
    Return:
        ProvinceD: Searched province.
    """

    return session.get(ProvinceD, province_id)
