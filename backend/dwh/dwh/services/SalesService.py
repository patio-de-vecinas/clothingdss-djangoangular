import time

from dwh.models.models import *
from dwh.services.dto.SalesFilterDTO import *
from sqlalchemy import asc, and_
from sqlalchemy.sql import func

from dwh.db import session, engine
from dwh.models.models import SalesH
from dwh.services.DateInterpreter import dbdate_from_frontdate


def get_sales(dto: SalesFilterDTO):
    geography_value = dto.get_geography_filter_value()
    # Product identifiers are obtained according to the filters
    product_query = f"""SELECT P.cod_product FROM \"Product_D\" P WHERE 1 = 1"""
    if dto.get_prod_prod() is not None:
        product_query = product_query + " AND P.name_prod = '" + dto.get_prod_prod()[0] + "'"
    if dto.get_prod_colour() is not None:
        product_query = product_query + " AND P.colour = '" + dto.get_prod_colour()[0] + "'"
    if dto.get_prod_size() is not None:
        product_query = product_query + " AND P.size = '" + dto.get_prod_size()[0] + "'"
    if dto.get_prod_brand() is not None:
        product_query = product_query + " AND P.brand = '" + dto.get_prod_brand()[0] + "'"
    products_ids = [item[0] for item in engine.execute(product_query).all()]

    geography_hierarchy = dto.get_geography_hierarchy()
    if geography_hierarchy == "village":
        # Village tuple is obtained by the name of the filter
        village = session.query(VillageD).filter(VillageD.cod_village == geography_value).all()[0]
        # Sales are obtained with the data of the product identifiers and the data of the other filters.
        sales = session.query(SalesH) \
            .filter(SalesH.cod_shop == session.query(ShopD)
                    .filter(ShopD.cod_location == village.cod_village).all()[0].cod_shop) \
            .filter(SalesH.cod_time <= dto.get_end_date()) \
            .filter(dto.get_start_date() <= SalesH.cod_time) \
            .filter(SalesH.cod_product.in_(products_ids)).all()
        return sales
    elif geography_hierarchy == "province":
        # Province tuple is obtained by the name of the filter
        provinces = session.query(ProvinceD).filter(ProvinceD.cod_province == geography_value).all()
    elif geography_hierarchy == "region":
        # Region tuple is obtained by the name of the filter
        region = session.query(RegionD).filter(RegionD.cod_region == geography_value).all()
        province_value = region[0].cod_region
        provinces = session.query(ProvinceD).filter(ProvinceD.cod_region == province_value).all()
    elif geography_hierarchy == "country":
        # Country tuple is obtained by the name of the filter
        country = session.query(CountryD).filter(CountryD.cod_country == geography_value).all()
        list_of_regions = country[0].regions
        provinces = []
        for region in list_of_regions:
            provinces_of_region = region.provinces
            provinces.append(provinces_of_region)
        # Gets all provinces of a country
        provinces = [item for sublist in provinces for item in sublist]
    list_of_sales = []
    # Gets all the sales of the provinces that match the filters
    for province in provinces:
        for city in province.cities:
            sales = session.query(SalesH) \
                .filter(SalesH.cod_shop == session.query(ShopD)
                        .filter(ShopD.cod_location == city.cod_village).all()[0].cod_shop) \
                .filter(SalesH.cod_time <= dto.get_end_date()) \
                .filter(dto.get_start_date() <= SalesH.cod_time) \
                .filter(SalesH.cod_product.in_(products_ids)).all()
            list_of_sales.append(sales)
    return [item for sublist in list_of_sales for item in sublist]


def get_chart_information(year: int, month: int, cod_country: str):
    chart_query = f"""
    SELECT
        SUM(S.sales), R.name_region
    FROM "Sales_H" S
        JOIN "Shop_D" Sh ON S.cod_shop = Sh.cod_shop
            JOIN "Village_D" V ON Sh.cod_location = V.cod_village
            JOIN "Province_D" P ON V.cod_province = P.cod_province
            JOIN "Region_D" R ON P.cod_region = R.cod_region
        JOIN "Time_D" T ON S.cod_time = T.cod_time
    WHERE
        T.month = {month}
        AND T.year = {year}
        AND R.cod_country = '{cod_country}'
    GROUP BY
        R.name_region;
    """
    return [{"region": item[1], "value": item[0]} for item in engine.execute(chart_query).all()]


# Default page size
page_size_default = 10


# Interpretation queries
def get_pk_from_id(id: str) -> dict:
    values = id.split("_")
    return {
        "cod_product": values[0],
        "cod_time": values[1],
        "cod_shop": values[2]
    }


def get_id_from_pk(pk: dict) -> str:
    if "cod_product" in pk and "cod_time" in pk and "cod_shop" in pk:
        return f"{pk.get('cod_product')}_{pk.get('cod_time')}_{pk.get('cod_shop')}"
    return ""


# DB queries
def get_sales_page(page: int, page_size: int = page_size_default) -> list[SalesH]:
    initial_position = page * page_size
    final_position = initial_position + page_size
    return session \
        .query(SalesH) \
        .order_by(asc(SalesH.cod_product), asc(SalesH.cod_time), asc(SalesH.cod_shop)) \
        .slice(initial_position, final_position) \
        .all()


def get_sale_by_id(sale_id: str) -> SalesH:
    pk = get_pk_from_id(sale_id)
    cod_product = pk.get('cod_product')
    cod_time = dbdate_from_frontdate(pk.get('cod_time'))
    cod_shop = pk.get('cod_shop')
    return session.query(SalesH).filter(
        and_(
            SalesH.cod_product == cod_product,
            SalesH.cod_time == cod_time,
            SalesH.cod_shop == cod_shop
        )
    ).first()
