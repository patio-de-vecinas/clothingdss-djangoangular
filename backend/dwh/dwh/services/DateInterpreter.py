def dbdate_from_frontdate(frontdate: str):
    date = frontdate.split("-")
    return f"{date[2]}{date[1]}{date[0]}"
