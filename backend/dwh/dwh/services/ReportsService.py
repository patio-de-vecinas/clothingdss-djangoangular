import uuid
import openpyxl
import os
import shutil

from dwh.db import engine
from dwh.settings import BASE_DIR


def create_name_report():
    """Create name for new report with UUIDv4 encoding.
    Args:
    Return:
         str: Name of report.
    """
    return uuid.uuid4()


def create_report(year, month, fortnight):
    """Create new report.
    Args:
        month(Integer): Month's number of report.
        year(Integer): Year's number of report.
        fortnight(Integer): Fortnight's option of report.
    Return:
         str: Name of report.
    """
    query_year = year
    query_month = month
    if fortnight == 1:
        query_day_init = 1
        query_day_finish = 15
    else:
        query_day_init = 16
        query_day_finish = 31

    name_report = str(create_name_report()) + '.xlsx'

    files_dir = os.path.join(BASE_DIR, 'dwh', 'reports')

    if fortnight == 1:
        origin_file_dir = os.path.join(files_dir, 'Plantilla_1_15.xlsx')
    else:
        origin_file_dir = os.path.join(files_dir, 'Plantilla_16_31.xlsx')

    destiny_file_dir = os.path.join(files_dir, name_report)

    shutil.copy(origin_file_dir, destiny_file_dir)

    report_file = openpyxl.load_workbook(destiny_file_dir)

    sheet = report_file.get_sheet_by_name('Datos')

    data = engine.execute(f"""SELECT *
    FROM \"Sales_H\" S
        JOIN \"Time_D\" T ON S.cod_time = T.cod_time
        JOIN \"Product_D\" P ON P.cod_product = S.cod_product
        JOIN \"Shop_D\" Sh ON SH.cod_shop = S.cod_shop
        JOIN \"Village_D" V ON V.cod_village = Sh.cod_location
        JOIN \"Province_D\" Pr ON V.cod_province = Pr.cod_province
        JOIN \"Region_D\" R ON R.cod_region = Pr.cod_region
        JOIN \"Country_D" C ON C.cod_country = R.cod_country
    WHERE 
        T.month = {query_month} AND T.year = {query_year} AND T.day >= {query_day_init} AND T.day <= {query_day_finish}
    """)

    row = 2

    item = data.fetchone()
    while item is not None:
        # Order of fields in query
        # cod_product 0| cod_time 1| cod_shop 2| sales 3| stocks 4| cod_time 5| day 6| week 7| month 8| trimester 9|
        # year 10| cod_product 11| name_prod 12| colour 13|size 14| brand 15| price 16| cod_shop 17| name_shop 18|
        # cod_location 19| cod_village 20| city 21| cod_province 22| cod_province 23|
        # name_province 24| cod_region 25| cod_region 26| name_region 27| cod_country 28| cod_country 29|
        # name_country 30

        sheet.cell(column=1, row=row, value=item[12])
        sheet.cell(column=2, row=row, value=item[13])
        sheet.cell(column=3, row=row, value=item[14])
        sheet.cell(column=4, row=row, value=item[15])
        sheet.cell(column=5, row=row, value=item[16])
        sheet.cell(column=6, row=row, value=item[6])
        sheet.cell(column=7, row=row, value=item[7])
        sheet.cell(column=8, row=row, value=item[8])
        sheet.cell(column=9, row=row, value=item[9])
        sheet.cell(column=10, row=row, value=item[10])
        sheet.cell(column=11, row=row, value=item[18])
        sheet.cell(column=12, row=row, value=item[21])
        sheet.cell(column=13, row=row, value=item[24])
        sheet.cell(column=14, row=row, value=item[27])
        sheet.cell(column=15, row=row, value=item[30])
        sheet.cell(column=16, row=row, value=item[3])
        sheet.cell(column=17, row=row, value=item[4])

        row = row + 1
        item = data.fetchone()

    report_file.save(destiny_file_dir)
    report_file.close()

    return name_report
