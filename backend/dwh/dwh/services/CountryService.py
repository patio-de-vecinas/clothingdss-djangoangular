from sqlalchemy import asc

from dwh.db import session
from dwh.models.models import CountryD


def get_all_countries() -> list[CountryD]:
    """Gets all existing countries in the database.
    Args:
    Return:
         list[CountryD]: List of countries.
    """

    return session.query(CountryD).order_by(asc(CountryD.cod_country)).all()


def get_country_by_id(country_id: str) -> CountryD:
    """Gets existing country by identifier.
    Args:
        country_id(String): Country identifier
    Return:
        CountryD: Searched country.
    """

    return session.get(CountryD, country_id)
