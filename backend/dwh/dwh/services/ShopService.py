from sqlalchemy import asc

from dwh.db import session
from dwh.models.models import ShopD


def get_all_shops() -> list[ShopD]:
    """Gets all existing shops in the database.
    Args:
    Return:
         list[ShopD]: List of shops.
    """

    return session.query(ShopD).order_by(asc(ShopD.cod_shop)).all()


def get_shop_by_id(shop_id: int) -> ShopD:
    """Gets existing shop by identifier.
    Args:
        shop_id(Integer): Shop identifier
    Return:
        ShopD: Searched shop.
    """

    return session.get(ShopD, shop_id)
