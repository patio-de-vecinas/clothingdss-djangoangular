from sqlalchemy import asc

from dwh.db import session
from dwh.models.models import VillageD


def get_all_villages() -> list[VillageD]:
    """Gets all existing villages in the database.
    Args:
    Return:
         list[VillageD]: List of villages.
    """

    return session.query(VillageD).order_by(asc(VillageD.cod_village)).all()


def get_village_by_id(village_id: int) -> VillageD:
    """Gets existing village by identifier.
    Args:
        village_id(Integer): Village identifier
    Return:
        VillageD: Searched village.
    """

    return session.get(VillageD, village_id)
