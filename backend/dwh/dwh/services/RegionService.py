from sqlalchemy import asc

from dwh.db import session
from dwh.models.models import RegionD


def get_all_regions() -> list[RegionD]:
    """Gets all existing regions in the database.
    Args:
    Return:
         list[RegionD]: List of regions.
    """

    return session.query(RegionD).order_by(asc(RegionD.cod_region)).all()


def get_region_by_id(region_id: int) -> RegionD:
    """Gets existing region by identifier.
    Args:
        region_id(Integer): Region identifier
    Return:
        RegionD: Searched region.
    """

    return session.get(RegionD, region_id)
