from sqlalchemy import asc, desc

from dwh.db import session
from dwh.models.models import TimeD


def get_all_times() -> list[TimeD]:
    """Gets all existing times in the database.
    Args:
    Return:
         list[TimeD]: List of times.
    """

    return session.query(TimeD).order_by(asc(TimeD.cod_time)).all()


def get_all_years() -> list[int]:
    """Gets all existing years in the database
    Return:
        list[int]: List of years.
    """

    list = session.query(TimeD.year).distinct().order_by(desc(TimeD.year)).all()
    return [elem[0] for elem in list]


def get_time_by_id(time_id: int) -> TimeD:
    """Gets existing time by identifier.
    Args:
        time_id(Integer): Time identifier.
    Return:
        TimeD: Searched time.
    """

    return session.get(TimeD, time_id)
