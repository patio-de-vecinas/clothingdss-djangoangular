"""dwh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include

from dwh.views import CountryViews, RegionViews, ProvinceViews, VillageViews, ShopViews, TimeViews, ProductViews, SalesViews, ReportsViews, InterestingFactsViews

urlpatterns = [
    path(r'api/countries/', include(CountryViews.urlpatterns)),
    path(r'api/regions/', include(RegionViews.urlpatterns)),
    path(r'api/provinces/', include(ProvinceViews.urlpatterns)),
    path(r'api/villages/', include(VillageViews.urlpatterns)),
    path(r'api/shops/', include(ShopViews.urlpatterns)),
    path(r'api/times/', include(TimeViews.urlpatterns)),
    path(r'api/products/', include(ProductViews.urlpatterns)),
    path(r'api/sales/', include(SalesViews.urlpatterns)),
    path(r'api/reports/', include(ReportsViews.urlpatterns)),
    path(r'reports/download/<str:name>', ReportsViews.download),
    path(r'api/fun-fact', include(InterestingFactsViews.urlpatterns))
]
