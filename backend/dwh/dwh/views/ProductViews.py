from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.ProductSerializer import ProductSerializer
from dwh.services import ProductService
from dwh.serializers.ProductPropertiesDictSerializer import ProductPropertiesDictSerializer


@api_view(['GET'])
def get_all_products(_):
    """API REST Get operation for the total number of products.
    Args:
        _(Request): Product HTTP request object.
    Return:
        Response: HTTP response with product search result.
    """

    products_list = ProductService.get_all_products()
    if len(products_list) > 0:
        serializer = ProductSerializer(products_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_product(_, product_id: int):
    """API REST Get operation for a product resource.
    Args:
        _(Request): Product HTTP request object.
        product_id(Integer): Product identifier.
    Return:
        Response: HTTP response with product search result.
    """

    product = ProductService.get_product_by_id(product_id)
    if product:
        serializer = ProductSerializer(product, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_properties(_):
    """API REST Get operation for the possible values for the fields of a product.
    Args:
        _(Request): Product HTTP request object.
    Return:
        Response: HTTP response with possible fields search result.
    """

    possibilities = ProductService.get_properties()
    if possibilities:
        serializer = ProductPropertiesDictSerializer(possibilities)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


urlpatterns = [
    path('', get_all_products),
    path('<int:product_id>', get_one_product),
    path('properties', get_properties)
]
