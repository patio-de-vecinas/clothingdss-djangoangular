from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.CountrySerializer import CountrySerializer
from dwh.services import CountryService


@api_view(['GET'])
def get_all_countries(_):
    """API REST Get operation for the total number of countries.
    Args:
        _(Request): Country HTTP request object.
    Return:
        Response: HTTP response with country search result.
    """

    countries_list = CountryService.get_all_countries()
    if len(countries_list) > 0:
        serializer = CountrySerializer(countries_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_country(_, country_id: str):
    """API REST Get operation for a country resource.
    Args:
        _(Request): Country HTTP request object.
        country_id(String): Country identifier.
    Return:
        Response: HTTP response with country search result.
    """

    country = CountryService.get_country_by_id(country_id)
    if country:
        serializer = CountrySerializer(country, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_all_countries),
    path('<str:country_id>', get_one_country)
]
