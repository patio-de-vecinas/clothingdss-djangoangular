from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.services import InterestingFactsService


@api_view(['GET'])
def get_interesting_fact(_):
    """API REST Get random interesting fact.
    Args:
        _(Request): HTTP request object.
    Return:
        Response: HTTP response with random interesting fact.
    """

    interesting_fact = InterestingFactsService.get_interesting_fact()
    if interesting_fact:
        return Response(interesting_fact)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_interesting_fact),
]
