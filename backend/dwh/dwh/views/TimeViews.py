from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.TimeSerializer import TimeSerializer
from dwh.services import TimeService


@api_view(['GET'])
def get_all_times(_):
    """API REST Get operation for the total number of times.
    Args:
        _(Request): Time HTTP request object.
    Return:
        Response: HTTP response with time search result.
    """

    times_list = TimeService.get_all_times()
    if len(times_list) > 0:
        serializer = TimeSerializer(times_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_all_years(_):
    """API REST Get operation for all available years in DB.
        Args:
            _(Request): Time HTTP request object.
        Return:
            Response: HTTP response with year search result.
        """
    years_list = TimeService.get_all_years()
    if len(years_list) > 0:
        return Response(years_list)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_time(_, time_id: int):
    """API REST Get operation for a time resource.
    Args:
        _(Request): Time HTTP request object.
        time_id(Integer): Time identifier.
    Return:
        Response: HTTP response with time search result.
    """

    time = TimeService.get_time_by_id(time_id)
    if time:
        serializer = TimeSerializer(time, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_all_times),
    path('years/', get_all_years),
    path('<int:time_id>', get_one_time)
]
