from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from dwh.serializers.SalesSerializer import SalesSerializer
from dwh.serializers.ChartInfoSerializer import ChartInfoSerializer
from dwh.services import SalesService
from dwh.services.dto.SalesFilterDTO import SalesFilterDTO


@api_view(['GET'])
def get_sales_page(request: Request, page: int):
    num_page = page
    page_size = request.query_params.get("size")
    if num_page is not None:
        try:
            if page_size is None:
                sales = SalesService.get_sales_page(int(num_page))
            else:
                sales = SalesService.get_sales_page(int(num_page), int(page_size))
            serializer = SalesSerializer(sales, many=True)
            return Response(serializer.data)
        except ValueError:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def get_filtered_sales(request: Request):
    dto = SalesFilterDTO()
    dto.fill_dto_from_request(request)
    sales = SalesService.get_sales(dto)
    if sales:
        serializer = SalesSerializer(sales, many=True)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_sale_by_id(_, sale_id: str):
    sale = SalesService.get_sale_by_id(sale_id)
    if sale:
        serializer = SalesSerializer(sale, many=False)
        print(sale)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_chart_info(_, year: int, month: int):
    info = SalesService.get_chart_information(year, month, 'ES')
    if info:
        serializer = ChartInfoSerializer(info, many=True)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


urlpatterns = [
    path('page/<int:page>', get_sales_page),
    path('chart/<int:year>/<int:month>', get_chart_info),
    path('', get_filtered_sales),
    path('<str:sale_id>', get_sale_by_id)
]
