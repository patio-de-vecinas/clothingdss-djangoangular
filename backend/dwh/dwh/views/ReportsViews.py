import os

from django.http import HttpResponse
from django.urls import path

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.services import ReportsService

from dwh.settings import BASE_DIR


@api_view(['GET'])
def get_report(_, year: int = None, month: int = None, fortnight: int = None):
    """API REST Get operation for a shop resource.
    Args:
        _(Request): HTTP request object.
        year (Integer): Year
        month (Integer): Month
        fortnight (Integer): Fortnight
    Return:
        Response: HTTP response with name of created report.
    """

    year_report = year
    month_report = month
    fortnight_report = fortnight

    report = ReportsService.create_report(year_report, month_report, fortnight_report)

    if report:
        return Response(report)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


def download(_, name):
    """Get operation for a report resource. Browser downloads report
    Args:
        _(Request): HTTP request object.
        name(String): Report identifier.
    Return:
        Response: HTTP response with report search result.
    """
    
    name_report = name
    files_dir = os.path.join(BASE_DIR, 'dwh', 'reports')
    report_dir = os.path.join(files_dir, name_report)

    report = open(report_dir, "rb").read()

    response = HttpResponse(report, content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response["Content-Length"] = os.path.getsize(report_dir)
    response["Content-Disposition"] = "attachment; filename=report.xlsx"

    os.remove(report_dir)

    return response


urlpatterns = [
    path(r'<int:year>/<int:month>/<int:fortnight>', get_report)
]
