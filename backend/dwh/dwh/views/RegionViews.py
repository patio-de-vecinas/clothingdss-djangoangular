from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.RegionSerializer import RegionSerializer
from dwh.services import RegionService


@api_view(['GET'])
def get_all_regions(_):
    """API REST Get operation for the total number of regions.
    Args:
        _(Request): Region HTTP request object.
    Return:
        Response: HTTP response with region search result.
    """

    regions_list = RegionService.get_all_regions()
    if len(regions_list) > 0:
        serializer = RegionSerializer(regions_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_region(_, region_id: int):
    """API REST Get operation for a region resource.
    Args:
        _(Request): Region HTTP request object.
        region_id(String): Region identifier.
    Return:
        Response: HTTP response with region search result.
    """

    region = RegionService.get_region_by_id(region_id)
    if region:
        serializer = RegionSerializer(region, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_all_regions),
    path('<int:region_id>', get_one_region)
]
