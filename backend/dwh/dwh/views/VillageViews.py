from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.VillageSerializer import VillageSerializer
from dwh.services import VillageService


@api_view(['GET'])
def get_all_villages(_):
    """API REST Get operation for the total number of villages.
    Args:
        _(Request): Village HTTP request object.
    Return:
        Response: HTTP response with village search result.
    """

    villages_list = VillageService.get_all_villages()
    if len(villages_list) > 0:
        serializer = VillageSerializer(villages_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_village(_, village_id: int):
    """API REST Get operation for a village resource.
    Args:
        _(Request): Village HTTP request object.
        village_id(Integer): Village identifier.
    Return:
        Response: HTTP response with village search result.
    """

    village = VillageService.get_village_by_id(village_id)
    if village:
        serializer = VillageSerializer(village, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_all_villages),
    path('<int:village_id>', get_one_village)
]