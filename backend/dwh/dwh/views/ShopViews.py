from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.ShopSerializer import ShopSerializer
from dwh.services import ShopService


@api_view(['GET'])
def get_all_shops(_):
    """API REST Get operation for the total number of shops.
    Args:
        _(Request): Shop HTTP request object.
    Return:
        Response: HTTP response with shop search result.
    """

    shops_list = ShopService.get_all_shops()
    if len(shops_list) > 0:
        serializer = ShopSerializer(shops_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_shop(_, shop_id: int):
    """API REST Get operation for a shop resource.
    Args:
        _(Request): Shop HTTP request object.
        shop_id(Integer): Shop identifier.
    Return:
        Response: HTTP response with shop search result.
    """

    shop = ShopService.get_shop_by_id(shop_id)
    if shop:
        serializer = ShopSerializer(shop, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_all_shops),
    path('<int:shop_id>', get_one_shop)
]
