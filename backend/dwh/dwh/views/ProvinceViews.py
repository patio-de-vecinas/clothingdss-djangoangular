from django.urls import path
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from dwh.serializers.ProvinceSerializer import ProvinceSerializer
from dwh.services import ProvinceService


@api_view(['GET'])
def get_all_provinces(_):
    """API REST Get operation for the total number of provinces.
    Args:
        _(Request): Province HTTP request object.
    Return:
        Response: HTTP response with province search result.
    """

    provinces_list = ProvinceService.get_all_provinces()
    if len(provinces_list) > 0:
        serializer = ProvinceSerializer(provinces_list, many=True)
        return Response(serializer.data)
    else:
        return Response([], status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_one_province(_, province_id: int):
    """API REST Get operation for a province resource.
    Args:
        _(Request): Province HTTP request object.
        province_id(Integer): Province identifier.
    Return:
        Response: HTTP response with province search result.
    """

    province = ProvinceService.get_province_by_id(province_id)
    if province:
        serializer = ProvinceSerializer(province, many=False)
        return Response(serializer.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


urlpatterns = [
    path('', get_all_provinces),
    path('<int:province_id>', get_one_province)
]
