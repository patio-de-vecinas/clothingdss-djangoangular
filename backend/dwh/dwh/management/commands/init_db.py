import itertools
import os
from datetime import datetime, timedelta
from random import randint

from django.core.management.base import BaseCommand
from dwh.settings import MY_BASE_DIR
from dwh.db import Base, engine, session
from dwh.models.models import CountryD, RegionD, ProvinceD, VillageD, ShopD, TimeD, ProductD, SalesH

EXTRACTOR = {'country': {'file': r'country_d.txt',
                         'generate_function_txt': 'create_countries_txt(file_path)',
                         'generate_function_bbdd': 'create_countries_bbdd(file_path)'
                         },
             'region': {'file': r'region_d.txt',
                        'generate_function_txt': 'create_regions_txt(file_path)',
                        'generate_function_bbdd': 'create_regions_bbdd(file_path)'
                        },
             'province': {'file': r'province_d.txt',
                          'generate_function_txt': 'create_provinces_txt(file_path)',
                          'generate_function_bbdd': 'create_provinces_bbdd(file_path)'
                          },
             'village': {'file': r'village_d.txt',
                           'generate_function_txt': 'create_villages_txt(file_path)',
                           'generate_function_bbdd': 'create_villages_bbdd(file_path)'
                           },
             'shop': {'file': r'shop_d.txt',
                      'generate_function_txt': 'create_shops_txt(file_path)',
                      'generate_function_bbdd': 'create_shops_bbdd(file_path)'
                      },
             'time': {'file': r'time_d.txt',
                      'generate_function_txt': 'create_times_txt(file_path)',
                      'generate_function_bbdd': 'create_times_bbdd(file_path)'
                      },
             'product': {'file': r'product_d.txt',
                         'generate_function_txt': 'create_products_txt(file_path)',
                         'generate_function_bbdd': 'create_products_bbdd(file_path)'
                         },
             }


class Command(BaseCommand):
    help = 'Command to generate data in a file and load it into the postgre database.'

    def add_arguments(self, parser):
        # Flags
        parser.add_argument('-g', '--generate_txt', action='store_true', help='It generates txt files with the data '
                                                                              'of each of the dimensions. A route can '
                                                                              'be specified with --route.')
        parser.add_argument('-f', '--force_generate_schema', action='store_true', help='Delete existing database '
                                                                                       'schemas and create new ones.')
        parser.add_argument('-c', '--fill_db', action='store_true', help='Load the database with all the information. '
                                                                         'A route can be specified with --route.')

        # Optional argument
        parser.add_argument('-r', '--route', type=str, help='Path of the dimension data files.')

    def handle(self, *args, **kwargs):
        generate_txt = kwargs['generate_txt']
        route = kwargs['route']
        force_generate_schema = kwargs['force_generate_schema']
        fill_db = kwargs['fill_db']

        if generate_txt:
            for table in EXTRACTOR.keys():
                if route:
                    generate_file(table, route)
                else:
                    generate_file(table)

                print(f"Data of {table} generated")

        if force_generate_schema:
            Base.metadata.drop_all(engine)
            Base.metadata.create_all(engine)

        if fill_db:
            for table in EXTRACTOR.keys():
                if route:
                    read_file(table, route)
                else:
                    read_file(table)
                print(f"Table of {table} filled")

            if route:
                create_sales_bbdd(route)
            else:
                create_sales_bbdd()


def generate_file(dimension_name, path=''):
    """Invoke the file generation functions for each dimension.
       Args:
            dimension_name(String): Name of the dimension from which data will be generated.
            path(String): Customised file path.
       Return:
    """
    if path != '':
        file_path = os.path.join(path, EXTRACTOR[dimension_name]['file'])
    else:
        file_path = os.path.join(MY_BASE_DIR, 'dataFiles', 'dimensionFiles',
                                 EXTRACTOR[dimension_name]['file'])

    eval(EXTRACTOR[dimension_name]['generate_function_txt'])


def create_countries_txt(file_path):
    """Data generator of the 'Country' dimension. Previously hardcoded data in file.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    pass


def create_regions_txt(file_path):
    """Data generator of the 'Region' dimension. Previously hardcoded data in file.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    pass


def create_provinces_txt(file_path):
    """Data generator of the 'Province' dimension. Previously hardcoded data in file.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    pass


def create_villages_txt(file_path):
    """Data generator of the 'Village' dimension. Previously hardcoded data in file.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    pass


def create_shops_txt(file_path):
    """Data generator of the 'Shop' dimension.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    names_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'shopFiles', 'shop_name.txt')
    names_list = []

    locations_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'dimensionFiles', 'village_d.txt')
    locations_list = []

    with open(names_file, 'r', encoding='UTF-8-sig') as name_file:
        lines_name_file = name_file.readlines()

    for name in lines_name_file:
        names_list.append(name.strip('\n'))

    with open(locations_file, 'r', encoding='UTF-8-sig') as location_file:
        lines_location_file = location_file.readlines()

    for location in lines_location_file:
        location = location.split(';')
        locations_list.append(location[0])

    shops_list = list(itertools.product(names_list, locations_list))

    with open(file_path, 'w+', encoding='UTF-8-sig') as file:
        cod = 1

        for shop in shops_list:
            cod_shop = str(cod).zfill(5)
            file.write(f"{cod_shop};{shop[0]};{shop[1]}\n")

            cod = cod + 1


def create_times_txt(file_path):
    """Data generator of the 'Time' dimension.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    # 1 -> Start date, end date and output route are requested to user
    print("Start date (format dd/mm/yyyy)")
    start_date = datetime.strptime('01/01/2019', '%d/%m/%Y')

    print("End date (format dd/mm/yyyy)")
    end_date = datetime.strptime('01/06/2022', '%d/%m/%Y')

    # Output file path
    route = file_path

    if start_date < end_date:
        with open(route, "w+") as file:
            # 2 -> Iteration over all dates between start and end dates
            while start_date <= end_date:
                # Printing date in file
                file.write(f"{start_date.strftime('%Y%m%d')};"
                           f"{start_date.strftime('%d')};"
                           f"{start_date.strftime('%W')};"
                           f"{start_date.strftime('%m')};"
                           f"Q{int((int(start_date.strftime('%m')) - 1) / 3 + 1)};"
                           f"{start_date.strftime('%Y')}\n")

                # Changing while condition
                start_date += timedelta(days=1)
    else:
        print("Error: end date is prior to start date")


def create_products_txt(file_path):
    """Data generator of the 'Producto' dimension.
       Args:
           file_path(String):Address of the file where the generated data is stored.
       Return:
    """
    names_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'productFiles', 'product_name.txt')
    names_list = []

    colours_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'productFiles', 'product_colour.txt')
    colours_list = []

    sizes_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'productFiles', 'product_size.txt')
    sizes_list = []

    brands_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'productFiles', 'product_brand.txt')
    brands_list = []

    with open(names_file, 'r', encoding='UTF-8-sig') as name_file:
        lines_name_file = name_file.readlines()

    for name in lines_name_file:
        names_list.append(name.strip('\n'))

    with open(colours_file, 'r', encoding='UTF-8-sig') as colour_file:
        lines_colour_file = colour_file.readlines()

    for colour in lines_colour_file:
        colours_list.append(colour.strip('\n'))

    with open(sizes_file, 'r', encoding='UTF-8-sig') as size_file:
        lines_size_file = size_file.readlines()

    for size in lines_size_file:
        sizes_list.append(size.strip('\n'))

    with open(brands_file, 'r', encoding='UTF-8-sig') as brand_file:
        lines_brand_file = brand_file.readlines()

    for brand in lines_brand_file:
        brands_list.append(brand.strip('\n'))

    products_list = list(itertools.product(names_list, colours_list, sizes_list, brands_list))

    with open(file_path, 'w+', encoding='UTF-8-sig') as file:
        cod = 1
        for product in products_list:
            cod_product = str(cod).zfill(5)
            price = randint(1000, 6000) / 100
            file.write(f"{cod_product};{product[0]};{product[1]};{product[2]};{product[3]};{price}\n")
            cod = cod + 1


def read_file(dimension_name, path=''):
    """Internal. To make files extractions and get data.
       Args:
           dimension_name(String): Name of the dimension from which data will be generated.
           path(String): Customised file path.
       Return:
    """
    if path != '':
        file_path = os.path.join(path, EXTRACTOR[dimension_name]['file'])
    else:
        file_path = os.path.join(MY_BASE_DIR, 'dataFiles', 'dimensionFiles',
                                 EXTRACTOR[dimension_name]['file'])

    eval(EXTRACTOR[dimension_name]['generate_function_bbdd'])


def create_countries_bbdd(file_path):
    """BBDD filler of the 'Country' dimension. Previously hardcoded data in file.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(CountryD(line[0], line[1]))

    session.commit()


def create_regions_bbdd(file_path):
    """BBDD filler of the 'Region' dimension. Previously hardcoded data in file.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(RegionD(int(line[0]), line[1], line[2]))

    session.commit()


def create_provinces_bbdd(file_path):
    """BBDD filler of the 'Province' dimension. Previously hardcoded data in file.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(ProvinceD(int(line[0]), line[1], int(line[2])))

    session.commit()


def create_villages_bbdd(file_path):
    """BBDD filler of the 'Village' dimension. Previously hardcoded data in file.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(VillageD(int(line[0]), line[1], int(line[2])))

    session.commit()


def create_shops_bbdd(file_path):
    """BBDD filler of the 'Shop' dimension.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(ShopD(int(line[0]), line[1], int(line[2])))

    session.commit()


def create_times_bbdd(file_path):
    """BBDD filler of the 'Time' dimension.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(TimeD(int(line[0]), int(line[1]), int(line[2]), int(line[3]), line[4], int(line[5])))

    session.commit()


def create_products_bbdd(file_path):
    """BBDD filler of the 'Product' dimension.
           Args:
               file_path(String):Address of the file where the data is stored.
           Return:
    """
    with open(file_path, 'r', encoding='UTF-8-sig') as file:
        for line in file:
            line = line.strip('\n').split(';')
            session.add(ProductD(int(line[0]), line[1], line[2], line[3], line[4], float(line[5])))

    session.commit()


def create_sales_bbdd(route=''):
    """'Sales fact data filler
       Args:
           route(String):Address of the file where the generated data is stored.
       Return:
    """
    if route != '':
        products_file = os.path.join(route, EXTRACTOR['product']['file'])
        times_file = os.path.join(route, EXTRACTOR['time']['file'])
        shops_file = os.path.join(route, EXTRACTOR['shop']['file'])
    else:
        products_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'dimensionFiles', EXTRACTOR['product']['file'])
        times_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'dimensionFiles', EXTRACTOR['time']['file'])
        shops_file = os.path.join(MY_BASE_DIR, 'dataFiles', 'dimensionFiles', EXTRACTOR['shop']['file'])

    products_cod_list = []
    times_cod_list = []
    shops_cod_list = []

    with open(products_file, 'r', encoding='UTF-8-sig') as product_file:
        for product in product_file:
            product = product.strip('\n').split(';')
            products_cod_list.append(product[0])

    with open(times_file, 'r', encoding='UTF-8-sig') as time_file:
        for time in time_file:
            time = time.strip('\n').split(';')
            times_cod_list.append(time[0])

    with open(shops_file, 'r', encoding='UTF-8-sig') as shop_file:
        for shop in shop_file:
            shop = shop.strip('\n').split(';')
            shops_cod_list.append(shop[0])

    stock = 100
    for product in products_cod_list:
        for time in times_cod_list:
            for shop in shops_cod_list:
                units_sold = randint(0, stock)
                session.add(SalesH(int(product), int(time), int(shop), int(units_sold), stock))
            session.commit()
