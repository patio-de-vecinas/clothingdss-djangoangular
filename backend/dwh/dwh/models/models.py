from sqlalchemy import Column, Integer, String, ForeignKey, Enum, Numeric
from sqlalchemy.orm import relationship, backref
from dwh.db import Base


class CountryD(Base):
    """Class implementing the Country dimension.
    Args:
        cod_country(String): Country code.
        name_country(String): Country name.
    """

    __tablename__ = 'Country_D'

    cod_country = Column(String(2), primary_key=True)
    name_country = Column(String(50), nullable=False)

    def __init__(self, cod_country, name_country):
        self.cod_country = cod_country
        self.name_country = name_country

    def __repr__(self):
        """Technical representation of the Country class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Country: {self.cod_country} {self.name_country}'

    def __str__(self):
        """End-user representation of the Country class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Country: {self.cod_country} {self.name_country}'

    def get_cod_country(self):
        """Gets the Country code.
        Args:
        Return:
            String: Country code.
        """

        return self.cod_country

    def get_name_country(self):
        """Gets the Country name.
        Args:
        Return:
            String: Country name.
        """

        return self.name_country

    def set_cod_country(self, cod_country):
        """Modifies the Country code.
        Args:
            cod_country(String): Country code.
        Return:
        """

        self.cod_country = cod_country

    def set_name_country(self, name_country):
        """Modifies the Country name.
        Args:
            name_country(String): Country name.
        Return:
        """

        self.name_country = name_country


class RegionD(Base):
    """Class implementing the Region dimension.
    Args:
        cod_region(Integer): Region code.
        name_region(String): Region name.
        cod_country(String): Country code reference.
    """

    __tablename__ = 'Region_D'

    cod_region = Column(Integer, primary_key=True)
    name_region = Column(String(50), nullable=False, index=True)
    cod_country = Column(String(2), ForeignKey('Country_D.cod_country'))
    country = relationship("CountryD", backref=backref('regions', lazy=False))

    def __init__(self, cod_region, name_region, cod_country):
        self.cod_region = cod_region
        self.name_region = name_region
        self.cod_country = cod_country

    def __repr__(self):
        """Technical representation of the Region class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Region: {self.cod_region} {self.name_region} {self.cod_country}'

    def __str__(self):
        """End-user representation of the Region class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Region: {self.cod_region} {self.name_region} {self.cod_country}'

    def get_cod_region(self):
        """Gets the Region code.
        Args:
        Return:
            Integer: Region code.
        """

        return self.cod_region

    def get_name_region(self):
        """Gets the Region name.
        Args:
        Return:
            String: Region name.
        """

        return self.name_region

    def get_cod_country(self):
        """Gets the Country code referenced.
        Args:
        Return:
            String: Country code referenced.
        """

        return self.cod_country

    def set_cod_region(self, cod_region):
        """Modifies the Region code.
        Args:
            cod_region(Integer): Region code.
        Return:
        """

        self.cod_region = cod_region

    def set_name_region(self, name_region):
        """Modifies the Region name.
        Args:
            name_region(String): Region name.
        Return:
        """

        self.name_region = name_region

    def set_cod_country(self, cod_country):
        """Modifies the Country code referenced.
        Args:
            cod_country(String): Country code referenced.
        Return:
        """

        self.cod_country = cod_country


class ProvinceD(Base):
    """Class implementing the Province dimension.
    Args:
        cod_province(Integer): Province code.
        name_province(String): Province name.
        cod_region(Integer): Region code reference.
    """

    __tablename__ = 'Province_D'

    cod_province = Column(Integer, primary_key=True)
    name_province = Column(String(50), nullable=False, index=True)
    cod_region = Column(Integer, ForeignKey('Region_D.cod_region'))
    region = relationship("RegionD", backref=backref('provinces', lazy=True))

    def __init__(self, cod_province, name_province, cod_region):
        self.cod_province = cod_province
        self.name_province = name_province
        self.cod_region = cod_region

    def __repr__(self):
        """Technical representation of the Province class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Province: {self.cod_province} {self.name_province} {self.cod_region}'

    def __str__(self):
        """End-user representation of the Province class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Province: {self.cod_province} {self.name_province} {self.cod_region}'

    def get_cod_province(self):
        """Gets the Province code.
        Args:
        Return:
            Integer: Province code.
        """

        return self.cod_province

    def get_name_province(self):
        """Gets the Province name.
        Args:
        Return:
            String: Province name.
        """

        return self.name_province

    def get_cod_region(self):
        """Gets the Region code referenced.
        Args:
        Return:
            Integer: Region code referenced.
        """

        return self.cod_region

    def set_cod_province(self, cod_province):
        """Modifies the Province code.
        Args:
            cod_province(Integer): Province code.
        Return:
        """

        self.cod_province = cod_province

    def set_name_province(self, name_province):
        """Modifies the Province name.
        Args:
            name_province(String): Province name.
        Return:
        """

        self.name_province = name_province

    def set_cod_region(self, cod_region):
        """Modifies the Region code referenced.
        Args:
            cod_region(Integer): Region code referenced.
        Return:
        """

        self.cod_region = cod_region


class VillageD(Base):
    """Class implementing the Village dimension.
    Args:
        cod_village(Integer): Village code.
        city(String): Village name.
        cod_province(Integer): Province code reference.
    """

    __tablename__ = 'Village_D'

    cod_village = Column(Integer, primary_key=True)
    city = Column(String(50), nullable=False, index=True)
    cod_province = Column(Integer, ForeignKey('Province_D.cod_province'))
    province = relationship("ProvinceD", backref=backref('cities', lazy=True))

    def __init__(self, cod_village, city, cod_province):
        self.cod_village = cod_village
        self.city = city
        self.cod_province = cod_province

    def __repr__(self):
        """Technical representation of the Village class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Village: {self.cod_village} {self.city} {self.cod_province}'

    def __str__(self):
        """End-user representation of the Village class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Village: {self.cod_village} {self.city} {self.cod_province}'

    def get_cod_village(self):
        """Gets the Village code.
        Args:
        Return:
            Integer: Village code.
        """

        return self.cod_village

    def get_city(self):
        """Gets the Village name.
        Args:
        Return:
            String: Village name.
        """

        return self.city

    def get_cod_province(self):
        """Gets the Province code referenced.
        Args:
        Return:
            Integer: Province code referenced.
        """

        return self.cod_province

    def set_cod_village(self, cod_village):
        """Modifies the Village code.
        Args:
            cod_village(Integer): Village code.
        Return:
        """

        self.cod_village = cod_village

    def set_city(self, city):
        """Modifies the Village name.
        Args:
            city(String): Village name.
        Return:
        """

        self.city = city

    def set_cod_province(self, cod_province):
        """Modifies the Province code referenced.
        Args:
            cod_province(Integer): Province code referenced.
        Return:
        """

        self.cod_province = cod_province


class ShopD(Base):
    """Class implementing the Shop dimension.
    Args:
        cod_shop(Integer): Shop code.
        name_shop(String): Shop name.
        cod_location(Integer): Village code reference.
    """

    __tablename__ = 'Shop_D'

    cod_shop = Column(Integer, primary_key=True)
    name_shop = Column(String(20), nullable=False)
    cod_location = Column(Integer, ForeignKey('Village_D.cod_village'))
    location = relationship("VillageD", backref=backref('shops', lazy=True))

    def __init__(self, cod_shop, name_shop, cod_location):
        self.cod_shop = cod_shop
        self.name_shop = name_shop
        self.cod_location = cod_location

    def __repr__(self):
        """Technical representation of the Shop class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Shop: {self.cod_shop} {self.name_shop} {self.cod_location}'

    def __str__(self):
        """End-user representation of the Shop class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Shop: {self.cod_shop} {self.name_shop} {self.cod_location}'

    def get_cod_shop(self):
        """Gets the Shop code.
        Args:
        Return:
            Integer: Shop code.
        """

        return self.cod_shop

    def get_name_shop(self):
        """Gets the Shop name.
        Args:
        Return:
            String: Shop name.
        """

        return self.name_shop

    def get_cod_location(self):
        """Gets the Village code referenced.
        Args:
        Return:
            Integer: Village code referenced.
        """

        return self.location

    def set_cod_shop(self, cod_shop):
        """Modifies the Shop code.
        Args:
            cod_shop(Integer): Shop code.
        Return:
        """

        self.cod_shop = cod_shop

    def set_name_shop(self, name_shop):
        """Modifies the Shop name.
        Args:
            name_shop(String): Shop name.
        Return:
        """

        self.name_shop = name_shop

    def set_cod_location(self, cod_location):
        """Modifies the Village code referenced.
        Args:
            cod_location(Integer): Village code referenced.
        Return:
        """

        self.cod_location = cod_location


class TimeD(Base):
    """Class implementing the Time dimension.
    Args:
        cod_time(Integer): Time code.
        day(Integer): Day number of the date.
        week(Integer): Week number of the date.
        month(Integer): Month number of the date.
        trimester(Enum): Trimester of the date.
        year(Integer): Year number of the date.
    """

    __tablename__ = 'Time_D'

    cod_time = Column(Integer, primary_key=True)
    day = Column(Integer, nullable=False)
    week = Column(Integer, nullable=False)
    month = Column(Integer, nullable=False, index=True)
    trimester = Column(Enum('Q1', 'Q2', 'Q3', 'Q4', name='Trimester'), nullable=False, index=True)
    year = Column(Integer, nullable=False, index=True)

    def __init__(self, cod_time, day, week, month, trimester, year):
        self.cod_time = cod_time
        self.day = day
        self.week = week
        self.month = month
        self.trimester = trimester
        self.year = year

    def __repr__(self):
        """Technical representation of the Time class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Time: {self.cod_time} {self.day} {self.week} {self.month} {self.trimester} {self.year}'

    def __str__(self):
        """End-user representation of the Time class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Time: {self.cod_time} {self.day} {self.week} {self.month} {self.trimester} {self.year}'

    def get_cod_time(self):
        """Gets the Time code.
        Args:
        Return:
            Integer: Time code.
        """

        return self.cod_time

    def get_day(self):
        """Gets the Time day.
        Args:
        Return:
            Integer: Time day.
        """

        return self.day

    def get_week(self):
        """Gets the Time week.
        Args:
        Return:
            Integer: Time week.
        """

        return self.week

    def get_month(self):
        """Gets the Time month.
        Args:
        Return:
            Integer: Time month.
        """

        return self.month

    def get_trimester(self):
        """Gets the Time trimester.
        Args:
        Return:
            Enum[String]: Time trimester.
        """

        return self.trimester

    def get_year(self):
        """Gets the Time year.
        Args:
        Return:
            Integer: Time year.
        """

        return self.year

    def set_cod_time(self, cod_time):
        """Modifies the Time code.
        Args:
            cod_time(Integer): Time code.
        Return:
        """

        self.cod_time = cod_time

    def set_day(self, day):
        """Modifies the Time day.
        Args:
            day(Integer): Time day.
        Return:
        """

        self.day = day

    def set_week(self, week):
        """Modifies the Time week.
        Args:
            week(Integer): Time week.
        Return:
        """

        self.week = week

    def set_month(self, month):
        """Modifies the Time month.
        Args:
            month(Integer): Time month.
        Return:
        """

        self.month = month

    def set_trimester(self, trimester):
        """Modifies the Time trimester.
        Args:
            trimester(String): Time trimester.
        Return:
        """

        self.trimester = trimester

    def set_year(self, year):
        """Modifies the Time year.
        Args:
            year(Integer): Time year.
        Return:
        """

        self.year = year


class ProductD(Base):
    """Class implementing the Product dimension.
    Args:
        cod_product(Integer): Product code.
        name_prod(String): Product name.
        colour(String): Product colour.
        size(Enum): Product size.
        brand(String): Product brand.
        price(Integer): Product price.
    """

    __tablename__ = 'Product_D'

    cod_product = Column(Integer, primary_key=True)
    name_prod = Column(String(20), nullable=False, index=True)
    colour = Column(String(20), nullable=False, index=True)
    size = Column(Enum('XS', 'S', 'M', 'L', 'XL', 'XXL', name='Size'), nullable=False, index=True)
    brand = Column(String(20), nullable=False, index=True)
    price = Column(Numeric(100, 2), nullable=False)

    def __init__(self, cod_product, name_prod, colour, size, brand, price):
        self.cod_product = cod_product
        self.name_prod = name_prod
        self.colour = colour
        self.size = size
        self.brand = brand
        self.price = price

    def __repr__(self):
        """Technical representation of the Product class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Product: {self.cod_product} {self.name_prod} {self.colour} {self.size} {self.brand} {self.price}'

    def __str__(self):
        """End-user representation of the Product class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Product: {self.cod_product} {self.name_prod} {self.colour} {self.size} {self.brand} {self.price}'

    def get_cod_product(self):
        """Gets the Product code.
        Args:
        Return:
            Integer: Product code.
        """

        return self.cod_product

    def get_name_prod(self):
        """Gets the Product name.
        Args:
        Return:
            String: Product name.
        """

        return self.name_prod

    def get_colour(self):
        """Gets the Product colour.
        Args:
        Return:
            String: Product colour.
        """

        return self.colour

    def get_size(self):
        """Gets the Product size.
        Args:
        Return:
            String: Product size.
        """

        return self.size

    def get_brand(self):
        """Gets the Product brand.
        Args:
        Return:
            String: Product brand.
        """

        return self.brand

    def get_price(self):
        """Gets the Product price.
        Args:
        Return:
            Integer: Product price.
        """

        return self.price

    def set_cod_product(self, cod_product):
        """Modifies the Product code.
        Args:
            cod_product(Integer): Product code.
        Return:
        """

        self.cod_product = cod_product

    def set_name_prod(self, name_prod):
        """Modifies the Product name.
        Args:
            name_prod(String): Product name.
        Return:
        """

        self.name_prod = name_prod

    def set_colour(self, colour):
        """Modifies the Product colour.
        Args:
            colour(String): Product colour.
        Return:
        """

        self.colour = colour

    def set_size(self, size):
        """Modifies the Product size.
        Args:
            size(String): Product size.
        Return:
        """

        self.size = size

    def set_brand(self, brand):
        """Modifies the Product brand.
        Args:
            brand(String): Product brand.
        Return:
        """

        self.brand = brand

    def set_price(self, price):
        """Modifies the Product price.
        Args:
            price(Integer): Product price.
        Return:
        """

        self.price = price


class SalesH(Base):
    """Class implementing the Sales event.
    Args:
        cod_product(Integer): Product code referenced.
        cod_time(Integer): Time code referenced.
        cod_shop(Integer): Shop code referenced.
        sales(Integer): Number of sales.
        stocks(Integer): Number of initial stocks.
    """

    __tablename__ = 'Sales_H'

    cod_product = Column(Integer, ForeignKey('Product_D.cod_product'), primary_key=True)
    product = relationship("ProductD", backref=backref('product', lazy=False))
    cod_time = Column(Integer, ForeignKey('Time_D.cod_time'), primary_key=True)
    time = relationship("TimeD", backref=backref('time', lazy=False))
    cod_shop = Column(Integer, ForeignKey('Shop_D.cod_shop'), primary_key=True)
    shop = relationship("ShopD", backref=backref('shop', lazy=False))
    sales = Column(Integer, nullable=False)
    stocks = Column(Integer, nullable=False)

    def __init__(self, cod_product, cod_time, cod_shop, sales, stocks):
        self.cod_product = cod_product
        self.cod_time = cod_time
        self.cod_shop = cod_shop
        self.sales = sales
        self.stocks = stocks

    def __repr__(self):
        """Technical representation of the Sale class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Sale: {self.cod_product} {self.cod_time} {self.cod_shop} {self.sales} {self.stocks}'

    def __str__(self):
        """End-user representation of the Sale class.
        Args:
        Return:
            String: Phrase with the object data.
        """

        return f'Sale: {self.cod_product} {self.cod_time} {self.cod_shop} {self.sales} {self.stocks}'

    def get_cod_product(self):
        """Gets the Product code referenced.
        Args:
        Return:
            Integer: Product code referenced.
        """

        return self.cod_product

    def get_cod_time(self):
        """Gets the Time code referenced.
        Args:
        Return:
            Integer: Time code referenced.
        """

        return self.cod_time

    def get_cod_shop(self):
        """Gets the Shop code referenced.
        Args:
        Return:
            Integer: Shop code referenced.
        """

        return self.cod_shop

    def get_sales(self):
        """Gets the Sale sales.
        Args:
        Return:
            Integer: Sale sales.
        """

        return self.sales

    def get_stocks(self):
        """Gets the Sale stocks.
        Args:
        Return:
            Integer: Sale stocks.
        """

        return self.stocks

    def set_cod_product(self, cod_product):
        """Modifies the Product code referenced.
        Args:
            cod_product(Integer): Product code referenced.
        Return:
        """

        self.cod_product = cod_product

    def set_cod_time(self, cod_time):
        """Modifies the Time code referenced.
        Args:
            cod_time(Integer): Time code referenced.
        Return:
        """

        self.cod_time = cod_time

    def set_cod_shop(self, cod_shop):
        """Modifies the Shop code referenced.
        Args:
            cod_shop(Integer): Shop code referenced.
        Return:
        """

        self.cod_shop = cod_shop

    def set_sales(self, sales):
        """Modifies the Sale sales.
        Args:
            sales(Integer): Sale sales.
        Return:
        """

        self.sales = sales

    def set_stocks(self, stocks):
        """Modifies the Sale stocks.
        Args:
            stocks(Integer): Sale stocks.
        Return:
        """

        self.stocks = stocks


class Chart():

    def __init__(self, name, data):
        self.name = name
        self.data = data

    def get_name(self):
        return self.name

    def get_data(self):
        return self.data

    def set_name(self, name):
        self.name = name

    def set_data(self, data):
        self.data = data
