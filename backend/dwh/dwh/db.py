from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgresql://<user>:<password>@<url>:<port>/<db_name>', echo=True)

Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()
