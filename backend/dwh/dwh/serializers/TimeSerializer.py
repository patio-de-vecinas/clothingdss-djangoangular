from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import TimeD


class TimeSerializer(serializers.ModelSerializer):
    """Class serializer of Time dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = TimeD
        session = session
        fields = '__all__'
