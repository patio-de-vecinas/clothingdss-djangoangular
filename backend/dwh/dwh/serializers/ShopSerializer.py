from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import ShopD


class ShopSerializer(serializers.ModelSerializer):
    """Class serializer of Shop dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = ShopD
        session = session
        fields = '__all__'
