from rest_framework import serializers


class ChartInfoSerializer(serializers.Serializer):
    """Class serializer of chart information object type.
    Args:
    """
    region = serializers.CharField(max_length=200)
    value = serializers.IntegerField()
