from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import VillageD


class VillageSerializer(serializers.ModelSerializer):
    """Class serializer of Village dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = VillageD
        session = session
        fields = '__all__'
