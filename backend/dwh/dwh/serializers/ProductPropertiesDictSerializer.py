from rest_framework import serializers


class ProductPropertiesDictSerializer(serializers.Serializer):
    """Class serializer of Product dimension properties.
    Args:
    """
    names = serializers.ListSerializer(child=serializers.CharField(max_length=200))
    colours = serializers.ListSerializer(child=serializers.CharField(max_length=200))
    sizes = serializers.ListSerializer(child=serializers.CharField(max_length=200))
    brands = serializers.ListSerializer(child=serializers.CharField(max_length=200))
