from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import ProvinceD


class ProvinceSerializer(serializers.ModelSerializer):
    """Class serializer of Province dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = ProvinceD
        session = session
        fields = '__all__'
