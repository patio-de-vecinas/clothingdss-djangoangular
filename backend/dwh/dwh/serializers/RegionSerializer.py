from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import RegionD


class RegionSerializer(serializers.ModelSerializer):
    """Class serializer of Region dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = RegionD
        session = session
        fields = '__all__'
