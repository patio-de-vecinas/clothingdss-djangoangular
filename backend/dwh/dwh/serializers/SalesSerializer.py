from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import SalesH


class SalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesH
        session = session
        fields = ['cod_product', 'cod_time', 'cod_shop', 'sales', 'stocks']
