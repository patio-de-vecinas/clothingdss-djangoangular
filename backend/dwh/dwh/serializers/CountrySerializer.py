from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import CountryD


class CountrySerializer(serializers.ModelSerializer):
    """Class serializer of Country dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = CountryD
        session = session
        fields = '__all__'
