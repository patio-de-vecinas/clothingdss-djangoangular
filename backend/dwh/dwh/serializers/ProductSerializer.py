from rest_witchcraft import serializers

from dwh.db import session
from dwh.models.models import ProductD


class ProductSerializer(serializers.ModelSerializer):
    """Class serializer of Product dimension instances.
    Args:
    """

    class Meta:
        """Class Meta intern logic serializer.
        Args:
        """

        model = ProductD
        session = session
        fields = '__all__'
