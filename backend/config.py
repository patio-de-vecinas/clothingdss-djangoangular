import os.path


class Config(object):
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
