import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartsPageComponent } from "./components/charts-page/charts-page.component";
import { FunFactsComponent } from './components/fun-facts/fun-facts.component';
import { QueryResultsDisplayComponent } from "./components/query-results-display/query-results-display.component";

const routes: Routes = [
  { path: '', component: QueryResultsDisplayComponent },
  { path: 'charts', component: ChartsPageComponent },
  { path: 'fun-fact', component: FunFactsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
