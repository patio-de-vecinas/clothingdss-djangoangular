import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CountryD } from '../models/CountryD.model';
import { ProductProperties } from '../models/ProductD.model';
import { SalesH } from '../models/SalesH.model';
import { CommonService } from './common.service';


@Injectable({
    providedIn: 'root',
})
export class TableService {
    constructor(private http: HttpClient, private common: CommonService) { }

    getResultsPage(url: string, page: number): Observable<SalesH[]> {
        return this.http.get<SalesH[]>(this.common.baseURL + url + '?page=' + page);
    }

    getCountry(cod_country: string): Observable<CountryD>{
        return this.http.get<CountryD>(this.common.baseURL + "/api/country/" + cod_country);
    }

    getProductsProperties(): Observable<ProductProperties> {
        return this.http.get<ProductProperties>(this.common.baseURL + "/api/products/properties");
    }
}
