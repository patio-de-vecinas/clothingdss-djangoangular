import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonService } from './common.service';
import { ExcelReportRequestDTO } from './dto/ExcelReportRequest.dto';

@Injectable({
    providedIn: 'root',
})
export class ExcelReportService {
    constructor(public http: HttpClient, private common: CommonService) { }

    getAvailableYears(): Observable<number[]> {
        return this.http.get<number[]>(this.common.baseURL + '/api/times/years');
    }

    generateExcelReport(dto: ExcelReportRequestDTO): Observable<string> {
        return this.http.get<string>(this.common.baseURL + `/api/reports/${dto.year}/${dto.month}/${dto.fortnight}`);
    }

    generateDownloadURL(docName: string): string {
        return this.common.baseURL + "/reports/download/" + docName;
    }
}
