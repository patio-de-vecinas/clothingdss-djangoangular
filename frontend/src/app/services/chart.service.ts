import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Chart } from '../models/Chart.model';
import { CountryD } from '../models/CountryD.model';
import { SalesH } from '../models/SalesH.model';
import { CommonService } from './common.service';

@Injectable({
    providedIn: 'root'
})
export class ChartService {

    constructor(private http: HttpClient, private common: CommonService) { }

    getAvailableYears(): Observable<number[]> {
        return this.http.get<number[]>(this.common.baseURL + '/api/times/years');
    }

    getChartData(year: number, month: number): Observable<Chart[]> {
        return this.http.get<Chart[]>(this.common.baseURL + "/api/sales/chart/" + year + "/" + month);
    }

}
