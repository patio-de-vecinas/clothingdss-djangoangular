export class ChartRequestDTO {
    _year!: number;
    _month!: number;

    constructor(year: number, month: number) {
        this.year = year;
        this.month = month;
    }

    get year(): number {
        return this._year;
    }

    set year(value: number) {
        this._year = value;
    }

    get month(): number {
        return this._month;
    }

    set month(value: number) {
        this._month = value;
    }
}