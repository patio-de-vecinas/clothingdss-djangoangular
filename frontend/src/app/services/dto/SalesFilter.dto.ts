import { DatePipe } from "@angular/common"
import { HttpParams } from "@angular/common/http";
import { FormGroup } from "@angular/forms"

const datePipe = new DatePipe('es-ES');
const transformDate = (date: Date) => datePipe.transform(date, 'dd-MM-yyyy') ?? undefined;

const getPreviousMonday = (date: Date) => {
    const dayOfWeek = date.getDay();
    const firstDayOfWeek = new Date(date);
    const diff = dayOfWeek >= 1 ? dayOfWeek - 1 : 6 - dayOfWeek;

    firstDayOfWeek.setDate(date.getDate() - diff)
    firstDayOfWeek.setHours(0, 0, 0, 0)

    return firstDayOfWeek
}

export class SalesFilterDTO {
    country?: string
    region!: string
    province?: string
    village?: string

    start_date?: string
    end_date?: string

    prod_size?: string
    prod_colour?: string
    prod_prod?: string
    prod_brand?: string


    fillWithFormGroup(formGroup: FormGroup): SalesFilterDTO {
        // Geography
        this.country = "ES";
        const region = formGroup.get("region")?.value;
        if (region !== "all"){
            this.region = region;
        }
        const province = formGroup.get("province")?.value;
        if (province !== "all"){
            this.province = province;
        }
        const village = formGroup.get("village")?.value;
        if (village !== "all"){
            this.village = village;
        }

        // Date
        const now = new Date();
        switch (formGroup.get("dateSelection")?.value) {
            case "all": {
                this.end_date = transformDate(now);
                break;
            }
            case "week": {
                this.start_date = transformDate(getPreviousMonday(now));
                this.end_date = transformDate(new Date(getPreviousMonday(now).setDate(getPreviousMonday(now).getDate() + 6)));
                break;
            }
            case "month": {
                this.start_date = transformDate(new Date(now.getFullYear(), now.getMonth(), 1));
                this.end_date = transformDate(new Date(now.getFullYear(), now.getMonth() + 1, 0));
                break;
            }
            case "quarter": {
                const quarterBeginning = new Date(now.getFullYear(), Math.floor(now.getMonth() / 3) * 3, 1);
                this.start_date = transformDate(quarterBeginning);
                this.end_date = transformDate(new Date(quarterBeginning.getFullYear(), quarterBeginning.getMonth() + 3, 0));
                break;
            }
            case "year": {
                this.start_date = transformDate(new Date(now.getFullYear(), 0, 1));
                this.end_date = transformDate(new Date(now.getFullYear() + 1, 0, 0));
                break;
            }
            case "date": {
                const date = transformDate(formGroup.get("date")?.value);
                this.start_date = date;
                this.end_date = date;
                break;
            }
            case "range": {
                this.start_date = transformDate(formGroup.get("range")?.get("start")?.value);
                this.end_date = transformDate(formGroup.get("range")?.get("end")?.value);
                break;
            }
        }

        // Product
        const prod_size = formGroup.get("product")?.get("size")?.value;
        if (prod_size !== "all"){
            this.prod_size = prod_size;
        }
        const prod_colour = formGroup.get("product")?.get("color")?.value;
        if (prod_colour !== "all"){
            this.prod_colour = prod_colour;
        }
        const prod_prod = formGroup.get("product")?.get("product")?.value;
        if (prod_prod !== "all"){
            this.prod_prod = prod_prod;
        }
        const prod_brand = formGroup.get("product")?.get("brand")?.value;
        if (prod_brand !== "all"){
            this.prod_brand = prod_brand;
        }

        return this;
    }

    getAsHttpParams(): HttpParams{
        let params = new HttpParams();

        // Geography
        if (this.country) params = params.set("country", this.country);
        if (this.region) params = params.set("region", this.region);
        if (this.province) params = params.set("province", this.province);
        if (this.village) params = params.set("village", this.village);

        // Time
        if (this.start_date) params = params.set("start_date", this.start_date);
        if (this.end_date) params = params.set("end_date", this.end_date);

        // Product
        if (this.prod_prod) params = params.set("prod_prod", this.prod_prod);
        if (this.prod_colour) params = params.set("prod_colour", this.prod_colour);
        if (this.prod_size) params = params.set("prod_size", this.prod_size);
        if (this.prod_brand) params = params.set("prod_brand", this.prod_brand);

        return params;
    }
}