export class ExcelReportRequestDTO {
    _year!: number;
    _month!: number;
    _fortnight!: number;

    constructor(year: number, month: number, fortnight: number) {
        this.year = year;
        this.month = month;
        this.fortnight = fortnight;
    }

    get year(): number {
        return this._year;
    }

    set year(value: number) {
        this._year = value;
    }

    get month(): number {
        return this._month;
    }

    set month(value: number) {
        this._month = value;
    }

    get fortnight(): number {
        return this._fortnight;
    }

    set fortnight(value: number) {
        this._fortnight = value;
    }
}
