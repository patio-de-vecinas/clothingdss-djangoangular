import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";


@Injectable({
    providedIn: 'root',
})
export class CommonService {
    // Base URL of REST API
    public baseURL: string;

    constructor(
        private window: Window
    ) {
        this.baseURL = '//' + this.window.location.hostname;
        if (this.window.location.port) {
            this.baseURL += ':' + (environment.production ? '8000' : '4200');
        }
    }
}
