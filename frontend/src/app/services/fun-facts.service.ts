import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { CommonService } from "./common.service";

@Injectable({
  providedIn: 'root'
})
export class FunFactsService {

    constructor(public http: HttpClient, private common: CommonService) { }

    getFunFact(): Observable<string> {
        return this.http.get<string>(this.common.baseURL + '/api/fun-fact');
    }
}
