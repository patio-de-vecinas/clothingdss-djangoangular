import { SalesFilterDTO } from 'src/app/services/dto/SalesFilter.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RegionD } from 'src/app/models/RegionD.model';
import { ProvinceD } from 'src/app/models/ProvinceD.model';
import { VillageD } from 'src/app/models/VillageD.model';
import { SalesH } from 'src/app/models/SalesH.model';

import { Observable } from 'rxjs';
import { ProductProperties } from '../models/ProductD.model';
import { CountryD } from '../models/CountryD.model';
import { CommonService } from './common.service';

@Injectable({ providedIn: 'root' })
export class ComunicationService {
    constructor(private httpClient: HttpClient, private common: CommonService) {}

    getSalesFilteredByDTO(dto: SalesFilterDTO): Observable<SalesH[]> {
        return this.httpClient.get<SalesH[]>(this.common.baseURL + '/api/sales', {
            params: dto.getAsHttpParams(),
        });
    }

    getAllProductProperties(): Observable<ProductProperties> {
        return this.httpClient.get<ProductProperties>(this.common.baseURL + '/api/products/properties');
    }

    getCountry(country: string): Observable<CountryD> {
        return this.httpClient.get<CountryD>(this.common.baseURL + '/api/countries/' + country);
    }

    getRegion(region: string): Observable<RegionD> {
        return this.httpClient.get<RegionD>(this.common.baseURL + '/api/regions/' + region);
    }

    getProvince(province: string): Observable<ProvinceD> {
        return this.httpClient.get<ProvinceD>(this.common.baseURL + '/api/provinces/' + province);
    }
}
