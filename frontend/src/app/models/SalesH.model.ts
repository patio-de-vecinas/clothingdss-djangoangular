export class SalesH {
    private _cod_product: number;
    private _cod_time: number;
    private _cod_shop: number;
    private _sales: number;
    private _stocks: number;

    constructor(
        cod_product: number,
        cod_time: number,
        cod_shop: number,
        sales: number,
        stocks: number
    ) {
        this._cod_product = cod_product;
        this._cod_time = cod_time;
        this._cod_shop = cod_shop;
        this._sales = sales;
        this._stocks = stocks;
    }

    get cod_product(): number {
        return this._cod_product;
    }

    set cod_product(value: number) {
        this._cod_product = value;
    }

    get cod_time(): number {
        return this._cod_time;
    }

    set cod_time(value: number) {
        this._cod_time = value;
    }

    get cod_shop(): number {
        return this._cod_shop;
    }

    set cod_shop(value: number) {
        this._cod_shop = value;
    }

    get sales(): number {
        return this._sales;
    }

    set sales(value: number) {
        this._sales = value;
    }

    get stocks(): number {
        return this._stocks;
    }

    set stocks(value: number) {
        this._stocks = value;
    }
}
