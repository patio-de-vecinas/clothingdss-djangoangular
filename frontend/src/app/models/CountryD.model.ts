import { RegionD } from "./RegionD.model";

export class CountryD {
    private _cod_country: number;
    private _name_country: string;
    private _regions: RegionD[];

    constructor(cod_country: number, name_country: string, regions: RegionD[]) {
        this._cod_country = cod_country;
        this._name_country = name_country;
        this._regions = regions;
    }

    get cod_country(): number {
        return this._cod_country;
    }

    set cod_country(value: number) {
        this._cod_country = value;
    }

    get name_country(): string {
        return this._name_country;
    }

    set name_country(value: string) {
        this._name_country = value;
    }
    get regions(): RegionD[] {
        return this._regions;
    }

    set regions(value: RegionD[]) {
        this._regions = value;
    }
}
