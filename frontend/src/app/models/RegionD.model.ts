import { ProvinceD } from "./ProvinceD.model";

export class RegionD {
    private _cod_region: number;
    private _name_region: string;
    private _cod_country: string;
    private _provinces: ProvinceD[];

    constructor(cod_region: number, name_region: string, cod_country: string, provinces: ProvinceD[]) {
        this._cod_region = cod_region;
        this._name_region = name_region;
        this._cod_country = cod_country;
        this._provinces = provinces;
    }

    get cod_region(): number {
        return this._cod_region;
    }

    set cod_region(value: number) {
        this._cod_region = value;
    }

    get name_region(): string {
        return this._name_region;
    }

    set name_region(value: string) {
        this._name_region = value;
    }

    get cod_country(): string {
        return this._cod_country;
    }

    set cod_country(value: string) {
        this._cod_country = value;
    }

    get provinces(): ProvinceD[] {
        return this._provinces;
    }

    set provinces(value: ProvinceD[]) {
        this._provinces = value;
    }
}
