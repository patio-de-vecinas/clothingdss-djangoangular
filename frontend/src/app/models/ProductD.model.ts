export class ProductD {
    private _cod_product: number;
    private _name_prod: string;
    private _color: string;
    private _size: string;
    private _brand: string;
    private _price: number;

    constructor(
        cod_product: number,
        name_prod: string,
        color: string,
        size: string,
        brand: string,
        price: number
    ) {
        this._cod_product = cod_product;
        this._name_prod = name_prod;
        this._color = color;
        this._size = size;
        this._brand = brand;
        this._price = price;
    }

    get cod_product(): number {
        return this._cod_product;
    }

    set cod_product(value: number) {
        this._cod_product = value;
    }

    get name_prod(): string {
        return this._name_prod;
    }

    set name_prod(value: string) {
        this._name_prod = value;
    }

    get color(): string {
        return this._color;
    }

    set color(value: string) {
        this._color = value;
    }

    get size(): string {
        return this._size;
    }

    set size(value: string) {
        this._size = value;
    }

    get sales(): number {
        return this.sales;
    }

    set sales(value: number) {
        this.sales = value;
    }

    get brand(): string {
        return this._brand;
    }

    set brand(value: string) {
        this._brand = value;
    }

    get price(): number {
        return this._price;
    }

    set price(value: number) {
        this._price = value;
    }
}

export interface ProductProperties{
    names: string[];
    colours: string[];
    sizes: string[];
    brands: string[];
}
