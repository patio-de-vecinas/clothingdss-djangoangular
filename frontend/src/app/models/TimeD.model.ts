export class TimeD {
    private _cod_time: number;
    private _day: number;
    private _week: number;
    private _month: number;
    private _trimester: string;
    private _year: number;

    constructor(cod_time: number, day: number, week: number, month: number, trimester: string, year: number) {
        this._cod_time = cod_time;
        this._day =  day;
        this._week =  week;
        this._month = month;
        this._trimester = trimester;
        this._year = year;
    }

    get cod_time(): number {
        return this._cod_time;
    }

    set cod_time(value: number) {
        this._cod_time = value;
    }

    get day(): number {
        return this._day;
    }

    set day(value: number) {
        this._day = value;
    }

    get week(): number {
        return this._week;
    }

    set week(value: number) {
        this._week = value;
    }

    get month(): number {
        return this._month;
    }

    set month(value: number) {
        this._month = value;
    }

    get trimester(): string {
        return this._trimester;
    }

    set trimester(value: string) {
        this._trimester = value;
    }

    get year(): number {
        return this._year;
    }

    set year(value: number) {
        this._year = value;
    }

}