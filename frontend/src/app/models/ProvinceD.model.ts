import { VillageD } from "./VillageD.model";

export class ProvinceD {
    private _cod_province: number;
    private _name_province: string;
    private _cod_region: number;
    private _cities: VillageD[];

    constructor(
        cod_province: number,
        name_province: string,
        cod_region: number,
        cities: VillageD[]
    ) {
        this._cod_province = cod_province;
        this._name_province = name_province;
        this._cod_region = cod_region;
        this._cities = cities;
    }

    get cod_province(): number {
        return this._cod_province;
    }

    set cod_province(value: number) {
        this._cod_province = value;
    }

    get name_province(): string {
        return this._name_province;
    }

    set name_province(value: string) {
        this._name_province = value;
    }

    get cod_region(): number {
        return this._cod_region;
    }

    set cod_region(value: number) {
        this._cod_region = value;
    }

    get cities(): VillageD[] {
        return this._cities;
    }

    set cities(value: VillageD[]) {
        this._cities = value;
    }
}
