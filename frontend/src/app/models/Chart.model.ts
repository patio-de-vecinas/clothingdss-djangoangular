export class Chart {
    private _region: string;
    private _value: number;

    constructor(region: string, value: number) {
        this._region = region;
        this._value = value;
    }

    get region(): string {
        return this._region;
    }

    set region(value: string) {
        this._region = value;
    }
    get value(): number {
        return this._value;
    }

    set value(value: number) {
        this._value = value;
    }

}
