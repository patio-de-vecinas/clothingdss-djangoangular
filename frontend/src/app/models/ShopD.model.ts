export class ShopD {
    private _cod_shop: number;
    private _name_shop: string;
    private _cod_location: number;

    constructor(cod_shop: number, name_shop: string, cod_location: number) {
        this._cod_shop = cod_shop;
        this._name_shop = name_shop;
        this._cod_location = cod_location;
    }

    get cod_shop(): number {
        return this._cod_shop;
    }

    set cod_shop(value: number) {
        this._cod_shop = value;
    }

    get name_shop(): string {
        return this._name_shop;
    }

    set name_shop(value: string) {
        this._name_shop = value;
    }

    get cod_ocation(): number {
        return this._cod_location;
    }

    set cod_location(value: number) {
        this._cod_location = value;
    }
}
