export class VillageD {
    private _cod_village: number;
    private _city: string;
    private _cod_province: number;

    constructor(cod_village: number, city: string, cod_province: number) {
        this._cod_village = cod_village;
        this._city = city;
        this._cod_province = cod_province;
    }

    get cod_village(): number {
        return this._cod_village;
    }

    set cod_village(value: number) {
        this._cod_village = value;
    }

    get city(): string {
        return this._city;
    }

    set city(value: string) {
        this._city = value;
    }

    get cod_province(): number {
        return this._cod_province;
    }

    set cod_province(value: number) {
        this._cod_province = value;
    }
}
