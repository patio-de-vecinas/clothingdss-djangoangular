import { ComunicationService } from 'src/app/services/comunication.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { SalesH } from 'src/app/models/SalesH.model';
import { SalesFilterDTO } from 'src/app/services/dto/SalesFilter.dto';
import { ProductProperties } from 'src/app/models/ProductD.model';
import { CountryD } from 'src/app/models/CountryD.model';
import { RegionD } from 'src/app/models/RegionD.model';
import { ProvinceD } from 'src/app/models/ProvinceD.model';
import { VillageD } from 'src/app/models/VillageD.model';

@Component({
    selector: 'app-portlet-filter',
    templateUrl: './portlet-filter.component.html',
    styleUrls: ['./portlet-filter.component.css'],
})
export class PortletFilterComponent implements OnInit {
    filterForm = this.fb.group({
        region: ['all'],
        province: [{ value: '', disabled: true }],
        village: [{ value: '', disabled: true }],
        dateSelection: ['all'],
        date: [{ value: '', disabled: true }],
        range: this.fb.group({
            start: [{ value: '', disabled: true }],
            end: [{ value: '', disabled: true }]
        }),
        product: this.fb.group({
            size: ['all'],
            color: ['all'],
            product: ['all'],
            brand: ['all'],
        }),
    });

    /*
        0 -> starting requests to backend
        2 -> ready to filter
    */
    status: number;

    dto!: SalesFilterDTO;
    listOfResults: SalesH[];

    productProperties!: ProductProperties;
    regions!: RegionD[];
    provinces!: ProvinceD[];
    villages!: VillageD[];

    errorLoading: boolean = false;


    @Output() fulfilledDTOEvent = new EventEmitter<SalesFilterDTO>();

    constructor(private fb: FormBuilder, private comunicationService: ComunicationService) {
        this.listOfResults = [];
        this.status = 0;
    }

    ngOnInit(): void {
        this.filterForm.get('region')?.valueChanges.subscribe(newValue => {
            if (newValue !== "all") {
                this.filterForm.get('province')?.enable();
                this.filterForm.get('province')?.setValue("all");
            } else {
                this.filterForm.get('province')?.disable();
                this.filterForm.get('province')?.setValue("");
                this.filterForm.get('village')?.disable();
                this.filterForm.get('village')?.setValue("");
            }
        });

        this.filterForm.get('province')?.valueChanges.subscribe(newValue => {
            if (newValue !== "all") {
                this.filterForm.get('village')?.enable();
                this.filterForm.get('village')?.setValue("all");
            } else {
                this.filterForm.get('village')?.disable();
                this.filterForm.get('village')?.setValue("");
            }
        });

        this.filterForm.get('dateSelection')?.valueChanges.subscribe(newValue => {
            if (newValue === "date") {
                this.filterForm.get('date')?.enable();
                this.filterForm.get('range')?.get('start')?.disable();
                this.filterForm.get('range')?.get('end')?.disable();
            } else if (newValue === "range") {
                this.filterForm.get('date')?.disable();
                this.filterForm.get('range')?.get('start')?.enable();
                this.filterForm.get('range')?.get('end')?.enable();
            } else {
                this.filterForm.get('date')?.disable();
                this.filterForm.get('range')?.get('start')?.disable();
                this.filterForm.get('range')?.get('end')?.disable();
            }
        });

        // Get properties from backend
        this.comunicationService.getAllProductProperties().subscribe({
            next: (properties: ProductProperties) => {
                this.productProperties = properties;
                this.status += 1;
            },
            error: _ => this.errorLoading = true
        });

        this.comunicationService.getCountry("ES").subscribe({
            next: (country: CountryD) => {
                this.regions = country.regions;
                this.status += 1;
            },
            error: _ => this.errorLoading = true
        })
    }

    sendFormDTO(): void {
        const dto = new SalesFilterDTO().fillWithFormGroup(this.filterForm);
        console.log(dto)
        this.fulfilledDTOEvent.emit(dto);
    }

    onRegionChange(): void {
        this.comunicationService.getRegion(this.filterForm.get("region")?.value).subscribe({
            next: (region: RegionD) => {
                this.provinces = region.provinces;
            },
            error: _ => this.errorLoading = true
        })
    }

    onProvinceChange(): void {
        this.comunicationService.getProvince(this.filterForm.get("province")?.value).subscribe({
            next: (province: ProvinceD) => {
                this.villages = province.cities;
            },
            error: _ => this.errorLoading = true
        })
    }
}
