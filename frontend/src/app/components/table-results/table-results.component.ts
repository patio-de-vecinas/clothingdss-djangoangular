import { Component, Input, OnInit } from '@angular/core';
import { SalesH } from 'src/app/models/SalesH.model';

@Component({
    selector: 'app-table-results',
    templateUrl: './table-results.component.html',
    styleUrls: ['./table-results.component.css'],
})
export class TableResultsComponent implements OnInit {

    // This list has to be loaded from parent component via HTTP
    @Input() listOfResults: SalesH[] = [];

    constructor() { }

    ngOnInit(): void { }
}
