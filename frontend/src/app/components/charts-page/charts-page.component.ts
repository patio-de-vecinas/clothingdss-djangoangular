import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-charts-page',
    templateUrl: './charts-page.component.html',
    styleUrls: ['./charts-page.component.css']
})
export class ChartsPageComponent implements OnInit {

    public numCharts: number;

    constructor() {
        this.numCharts = 1;
    }

    ngOnInit(): void {
    }

}
