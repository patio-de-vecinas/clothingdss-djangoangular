import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild
} from '@angular/core';
import { Modal } from 'bootstrap';
import * as moment from 'moment';
import { ExcelReportRequestDTO } from 'src/app/services/dto/ExcelReportRequest.dto';
import { ExcelReportService } from 'src/app/services/excel-report.service';


@Component({
    selector: 'app-download-excel-report',
    templateUrl: './download-excel-report.component.html',
    styleUrls: ['./download-excel-report.component.css'],
})
export class DownloadExcelReportComponent implements OnInit, AfterViewInit {
    @ViewChild('excelModal') excelModal!: ElementRef;
    public requestStatus: number;

    public bootstrapModal!: Modal;

    public dto!: ExcelReportRequestDTO;

    // Information coming from backend (via service)
    public years!: number[];
    public fileName!: string;

    // Information coming from moment
    public months: string[];

    constructor(public excelService: ExcelReportService) {
        moment.locale('es');
        this.dto = new ExcelReportRequestDTO(2022, 5, 1);

        /*
         * CÓDIGOS ESTADO DE PETICIÓN
         * 0 -> No hay nada pedido, se están buscando los años para imprimirlos
         * 1 -> Ya tengo los años, dejo elegir
         * 2 -> Informe pedido
         * 3 -> Nombre recibido
         */
        this.requestStatus = 0;
        this.months = moment.months();
    }

    ngOnInit(): void {
        this.excelService.getAvailableYears().subscribe({
            next: (data: number[]) => {
                this.years = data;
                this.requestStatus = 1;
            }
        });
    }

    ngAfterViewInit(): void {
        this.bootstrapModal = new Modal(this.excelModal.nativeElement, { backdrop: 'static' });
    }

    openModal(): void {
        this.bootstrapModal.show();
    }

    closeModal(): void {
        this.bootstrapModal.hide();
    }

    sendExcelRequest(): void {
        this.excelService.generateExcelReport(this.dto).subscribe({
            next: (fileName: any) => {
                this.fileName = fileName;
                this.requestStatus = 3;
            },
            error: (err: any) => {
                console.error(err);
                this.requestStatus = 5;
            }
        });
        this.requestStatus = 2;
    }

    downloaded(): void {
        this.requestStatus = 4;
    }
}
