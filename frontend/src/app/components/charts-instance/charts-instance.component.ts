import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import * as moment from 'moment';
import { BaseChartDirective } from 'ng2-charts';
import { Chart } from 'src/app/models/Chart.model';
import { ChartService } from 'src/app/services/chart.service';
import { ChartRequestDTO } from 'src/app/services/dto/ChartRequest.dto';

@Component({
    selector: 'app-charts-instance',
    templateUrl: './charts-instance.component.html',
    styleUrls: ['./charts-instance.component.css']
})
export class ChartsInstanceComponent implements OnInit {

    public chart = { status: 0 };

    @ViewChild(BaseChartDirective) chartt: BaseChartDirective | undefined;

    public barChartOptions: ChartConfiguration['options'] = {
        responsive: true,
        scales: {
            x: {},
            y: {}
        },
        plugins: {
            legend: {
                display: true
            }
        }
    };
    public barChartType: ChartType = 'bar';

    public barChartData!: ChartData<'bar'>;

    public months!: string[];
    public years!: number[];

    public dto!: ChartRequestDTO;

    constructor(private chartService: ChartService) {
        moment.locale('es');
        this.months = moment.months();
        this.chartService.getAvailableYears().subscribe({
            next: (years: number[]) => {
                this.years = years;
                this.chart.status = 1;
                this.dto = new ChartRequestDTO(this.years[0], 5);
            }
        });

    }

    ngOnInit() {

    }

    loadChart(month: number, year: number) {
        this.chart.status = 2;

        this.chartService.getChartData(year, month).subscribe({
            next: (data: Chart[]) => {
                this.barChartData = {
                    labels: data.map(elem => elem.region),
                    datasets: [
                        {
                            data: data.map(elem => elem.value),
                            label: this.months[month - 1] + " " + year,
                            backgroundColor: 'rgb(144, 164, 174)',
                            hoverBackgroundColor: 'rgb(96, 125, 139)'
                        }
                    ]
                }
                this.chart.status = 3;
            },
            error: _ => this.chart.status = -1
        });
    }
}
