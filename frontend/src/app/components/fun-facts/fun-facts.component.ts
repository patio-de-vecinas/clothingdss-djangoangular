import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FunFactsService } from "../../services/fun-facts.service";

@Component({
    selector: 'app-fun-facts',
    templateUrl: './fun-facts.component.html',
    styleUrls: ['./fun-facts.component.css']
})
export class FunFactsComponent implements OnInit {

    public requestStatus: number;
    public funFact!: string;

    @Output() onLoaded: EventEmitter<any> = new EventEmitter<any>();

    constructor(public funFactsService: FunFactsService) {
        this.requestStatus = 0;
    }

    ngOnInit(): void {
        this.funFactsService.getFunFact().subscribe({
            next: (data: string) =>{
                this.funFact = data.charAt(0).toLowerCase() + data.slice(1);
                this.requestStatus = 1;
                this.onLoaded.emit();
        }
        })
    }

}
