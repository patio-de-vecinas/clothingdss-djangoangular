import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-charts-filter',
    templateUrl: './charts-filter.component.html',
    styleUrls: ['./charts-filter.component.css']
})
export class ChartsFilterComponent implements OnInit {

    public numCharts!: number;

    @Output() changedNumberCharts = new EventEmitter<number>();

    constructor() {
        this.numCharts = 1;
    }

    ngOnInit(): void {
    }

}
