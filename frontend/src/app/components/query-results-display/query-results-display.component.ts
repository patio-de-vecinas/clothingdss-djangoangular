import { ComunicationService } from 'src/app/services/comunication.service';
import { Component, OnInit } from '@angular/core';
import { SalesH } from 'src/app/models/SalesH.model';
import { SalesFilterDTO } from 'src/app/services/dto/SalesFilter.dto';


@Component({
    selector: 'app-query-results-display',
    templateUrl: './query-results-display.component.html',
    styleUrls: ['./query-results-display.component.css'],
})
export class QueryResultsDisplayComponent implements OnInit {
    dto!: SalesFilterDTO;
    listOfResults: SalesH[];

    loading: boolean;
    showFunFact: boolean;

    constructor(private comunicationService: ComunicationService) {
        this.listOfResults = [];
        this.loading = false;
        this.showFunFact = false;
    }

    ngOnInit(): void { }

    sendRequest(dto: SalesFilterDTO) {
        // DTO received
        this.dto = dto;

        // Start loading
        this.loading = true;

        // HTTP Request sent get Sales
        this.comunicationService.getSalesFilteredByDTO(dto).subscribe({
            next: (salesList: SalesH[]) => {
                this.listOfResults = salesList;
                this.loading = false;
            },
            error: (err: any) => console.error(err),
        });
    }

    onLoadedFunFact(){
        console.log("Show fun fact");
        this.showFunFact = true;
    }
}
