import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DownloadExcelReportComponent } from '../download-excel-report/download-excel-report.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
    @ViewChild(DownloadExcelReportComponent)
    excelComponent!: DownloadExcelReportComponent;
    
    currentURL!: string;

    constructor(private router: Router) {
        this.router.events.subscribe({
            next: evt => {
                if (evt instanceof NavigationEnd){
                    this.currentURL = evt.url;
                }
            }
        })
    }

    ngOnInit(): void { }

    openExcelModal() {
        this.excelComponent.openModal();
    }
}
