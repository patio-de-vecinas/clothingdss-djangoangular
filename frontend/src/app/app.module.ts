import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import es from '@angular/common/locales/es';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartsFilterComponent } from './components/charts-filter/charts-filter.component';
import { ChartsInstanceComponent } from './components/charts-instance/charts-instance.component';
import { ChartsPageComponent } from './components/charts-page/charts-page.component';
import { DownloadExcelReportComponent } from './components/download-excel-report/download-excel-report.component';
import { FunFactsComponent } from './components/fun-facts/fun-facts.component';
import { HeaderComponent } from './components/header/header.component';
import { PortletFilterComponent } from './components/portlet-filter/portlet-filter.component';
import { QueryResultsDisplayComponent } from './components/query-results-display/query-results-display.component';
import { TableResultsComponent } from './components/table-results/table-results.component';
import { DEFAULT_TIMEOUT, TimeoutInterceptor } from './interceptors/timeout.interceptor';

registerLocaleData(es);

@NgModule({
    declarations: [
        AppComponent,
        QueryResultsDisplayComponent,
        PortletFilterComponent,
        TableResultsComponent,
        ChartsInstanceComponent,
        ChartsPageComponent,
        ChartsFilterComponent,
        HeaderComponent,
        DownloadExcelReportComponent,
        FunFactsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        NgChartsModule,
        HttpClientModule,
        FormsModule,
    ],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
        { provide: LOCALE_ID, useValue: 'es-ES' },
        { provide: Window, useValue: window },
        { provide: DEFAULT_TIMEOUT, useValue: 10000000 },
        { provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule { }
